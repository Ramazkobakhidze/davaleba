package com.example.davaleba1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        gilaki.setOnClickListener {
            d("button", "button activated")
            teqsti()
        }

    }


    private fun teqsti(){
        val number:Int = (-100..100).random()
        d("button", "$number")
        if (number%5 == 0){
            if (number/5 > 0) {
                teqsti.text = "Yes"
            }else{
                teqsti.text = "No"
            }
        }

    }


}